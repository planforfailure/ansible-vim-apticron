## Description
Installs Vim/Apticron on Debian/Ubuntu derivatives using the following components.

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html
- https://github.com/sjl/badwolf
- https://manpages.debian.org/testing/apticron/apticron.1.en.html
- https://github.com/sjl/badwolf/tree/master/colors
- https://vim.fandom.com/wiki/Example_vimrc


## Setup
Replace your own variables in `defaults/main.yml`. All other sensitive variables are encrypted via `ansible-vault encrypt passwd.yml`

## Role Variables
```yaml
---
user: foo
email_to: "updates@example.com"
custom_castrum_from: "no-reply@example.com"
custom_arcis_from: "no-reply@example.com"
```

## Dependencies
N/A

## Installation

```bash
$ ansible-playbook --extra-vars '@passwd.yml' deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-vim-apticron
```
## Licence

MIT/BSD
